--liquibase formatted sql

--changeset bugarchev:01 labels:DEV,PROD,release1.0.0
CREATE TABLE CARDS_R1(
                      id INT PRIMARY KEY,
                      name VARCHAR(30) NOT NULL
)


--changeset bugarchev:02 labels:test
CREATE TABLE ACCOUNTS_R1(
                      id INT PRIMARY KEY,
                      name VARCHAR(12) NOT NULL,
                      type VARCHAR(25)
)